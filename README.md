# Docker image for Java 8 with Alpine linux


## Build image manually

## Build arguments

  - PROXY         (Optional) specify proxy if run build behide HTTP Proxy
  - GLIBC_VERSION (Optional) specify glibc version number. Default is 2.27-r0
  - JAVA_VERSION  (Optional) Java major version. For example 8u172b11, JAVA_VERSION is 8
  - JAVA_UPDATE   (Optional) Java minor version. For example 8u172b11, JAVA_UPDATE is 172
  - JAVA_BUILD    (Optional) Java minor version. For example 8u172b11, JAVA_BUILD is 11
  - JAVA_PATH     (Optional) Oracle license agreement path number.


- Building JDK
  ```
  docker build -t <tag>:<version> -f ./jdk/Dockerfile ./jdk
  ```

- Building JRE
  ```
  docker build -t <tag>:<version> -f ./jre/Dockerfile ./jre
  ```

- Building SERVER-JRE
  ```
  docker build -t <tag>:<version> -f ./server-jre/Dockerfile ./server-jre
  ```

## Build image with Gitlab CI pipeline















**Reference**
https://developer.atlassian.com/blog/2015/08/minimal-java-docker-containers/

https://github.com/sgerrand/alpine-pkg-glibc

https://stackoverflow.com/questions/10268583/downloading-java-jdk-on-linux-via-wget-is-shown-license-page-instead
